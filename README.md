# LNChromaKey
Chromakeying two videos + addings effects

## Prerequisites
* Python >=3.6
* opencv-python
* numpy
* pillow
* pyscreenshot
* pychromakey
* ffmpeg

## Usage

* place videos named background.mp4 and foreground.mp4 in the res directory (both videos should be max 1080p with the same framerate and codec, higher resolutions are supported but untested - it depends on the power of the machine it's running on)
* run `python3 main.py` or `python main.py` in the LNChromakey directory
* use mouse cursor in preview window to select color to key out
* use RGB sliders in control windows to adjust color
* between RGBmin and RGBmax is the range of RGB values that get keyed out of the foreground video
* use Effects slider in control window to turn effects on / off
* set intensity for each effect with sliders below
* use space to start recording - this will reset both background and foreground video to the first frame and generate an mp4 / h264 video in the out directory
* use escape or Ctrl + C in terminal to exit

Modify `constants.py` to change defaults.

Project directory explained

LNChromaKey

```
├── constants.py
├── frames -> frames get saved here (delete manually between sessions if no longer needed)
├── LICENSE.md
├── main.py -> main code
├── main.spec
├── out -> encoded videos go here (will be named by date-time.mp4)
├── __pycache__
├── pychromakey -> python library
├── README.md
├── requirements.txt
├── res
└── setup.py
```
